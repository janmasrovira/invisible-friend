module Draw where

import           Control.Monad.Random
import           System.Random.Shuffle

pairs :: [t] -> Maybe [(t, t)]
pairs [] = Just []
pairs (a:b:xs) = ((a, b) :) <$> pairs xs
pairs _ = Nothing

rndPairs :: MonadRandom f => [t] -> f (Maybe [(t, t)])
rndPairs xs = pairs <$> shuffleM xs


f :: [a] -> Bool
f [] = True
