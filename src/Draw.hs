module Draw where

import           Control.Monad.Random
import           Data.List
import qualified Data.Set as Set

draw :: (Eq a, Ord a, MonadRandom m) => [a] -> m [(a, a)]
draw x = step (Set.fromList x) x []
   where step _ [] ac = return ac
         step bag (x:xs) ac = do
            ir <- head . filter ((/=x) . flip Set.elemAt bag) <$> getRandomRs (0, Set.size bag - 1)
            let y = Set.elemAt ir bag
            step (Set.deleteAt ir bag) xs ((x, y) : ac)
