module Mail where

import           Network.Mail.Client.Gmail
import           Network.Mail.Mime
import           Data.Text.Lazy
import qualified Data.Text as S

type UserName = Text
type Password = Text
type Subject = S.Text
type Body = Text

q :: IO ()
q = sendGmail spamUsr spamPwd
  spamAddr
  [janAddr] [] []
  "bon dia!" "msg" [] (10*1000*1000)

botSendGmail :: [Address] -> Subject -> Body -> IO ()
botSendGmail to subject body =
  sendGmail spamUsr spamPwd spamAddr
  to [] []
  subject body [] (10 ^ (7 :: Int))

janAddr :: Address
janAddr = Address (Just "Jan Mas Rovira") "janmasrovira@gmail.com"

spamAddr :: Address
spamAddr = Address Nothing "botspom74@gmail.com"

spamUsr :: UserName
spamUsr = "botspam74"

spamPwd :: Password
spamPwd = "spambot123"
