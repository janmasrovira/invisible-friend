module Parser where

import Network.Mail.Mime
import Data.Text as S
import Data.Text.IO as S

type Error = String

parseParticipantsFile :: FilePath -> IO (Either Error [Address])
parseParticipantsFile f = parseParticipants <$> S.readFile f

parseParticipants :: Text -> Either Error [Address]
parseParticipants = mapM parseParticipant . S.lines

-- the format is: My Full Name, myemail@gmail.com
parseParticipant :: Text -> Either Error Address
parseParticipant t = case splitOn ", " t of
  [name, addr] -> Right (Address (Just name) addr)
  _ -> Left "incorrect input"
